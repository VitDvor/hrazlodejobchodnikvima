﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{

    class Mapa
    {


        public char[,] Data;
        protected IGeneratorMapy generator;
        private Vysek posledni;

        public Mapa(IGeneratorMapy generator)
        {
            this.generator = generator;
        }

        public void Generuj(List<DataNakupujici> nakupujici, List<DataZlodej> zlodeji)
        {
            Data = generator.Generuj(80, 20, 100, 40);
            generator.PridejOsoby(nakupujici, Data);
            generator.PridejOsoby(zlodeji, Data);
        }

        public char[,] VratVysek(Souradnice s)
        {
            int rozmer = 2;
            char[,] vysek = new char[rozmer * 2 + 1, rozmer * 2 + 1];
            for (int i = 0; i < rozmer * 2 + 1; i++)
            {
                for (int j = 0; j < rozmer*2+1; j++)
                {
                    Souradnice relativni = new Souradnice(i - rozmer, j - rozmer);
                    vysek[i, j] = ZnakProPozici(s + relativni);
                }
            }
            Vysek v = new Vysek(vysek);
            NastavViditelnost(v);
            posledni = v;
            return v.Data;
        }

        public int this[Souradnice key]
        {
            get
            {
                return Data[key.X, key.Y];
            }
        }

        private Souradnice Vysek(Souradnice relativni, int kvadrant)
        {
            if (kvadrant == 2)
            { // o 90 proti smeru
                int t = relativni.X;
                relativni.X = -relativni.Y;
                relativni.Y = t;
            }
            if(kvadrant == 3)
            { // o 180
                relativni.X = -relativni.X;
                relativni.Y = -relativni.Y;
            }
            if(kvadrant == 4)
            { // o 90 po smeru
                int t = relativni.X;
                relativni.X = relativni.Y;
                relativni.Y = -t;
            }
            return relativni;
        }

        private Souradnice Vysek(int x, int y, int kvadrant)
        {
            return Vysek(new Souradnice(x, y), kvadrant);
        }

        protected void NastavViditelnost(Vysek v)
        {
            // prvni kvadrant
            for (int kvadrant = 1; kvadrant <= 4; kvadrant++)
            {
                if (v[Vysek(1,1,kvadrant)] == 'X')
                {
                    v.Nevidet(Vysek(2, 2, kvadrant));
                    v.Nevidet(Vysek(2, 1, kvadrant));
                    v.Nevidet(Vysek(1, 2, kvadrant));
                }
                if(v[Vysek(0, 1, kvadrant)] == 'X')
                {
                    v.Nevidet(Vysek(1,1,kvadrant));
                    for (int i = 0; i <= 2; i++)
                        v.Nevidet(Vysek(i, 2, kvadrant));
                }
                if(v[Vysek(1, 0, kvadrant)] == 'X')
                {
                    v.Nevidet(Vysek(1, 1, kvadrant));
                    for (int i = 0; i <= 2; i++)
                        v.Nevidet(Vysek(2, i, kvadrant));
                }
                if (v[Vysek(2, 1, kvadrant)] == 'X' || v[Vysek(1,2,kvadrant)] == 'X')
                    v.Nevidet(Vysek(2, 2, kvadrant));
            }
        }

        /// <summary>
        /// Vrati znak v <see cref="Data"/>. Pokud vypadava z indexu, pak vraci X;
        /// </summary>
        /// <param name="s">Souradnice, ktere ma vracet.</param>
        /// <returns>Znak na mape nebo X, pokud je to mimo mapu.</returns>
        protected char ZnakProPozici(Souradnice s)
        {
            if(s.X >=0 && s.X < Data.GetLength(0) && s.Y >= 0 && s.Y < Data.GetLength(1))
            {
                return Data[s.X, s.Y];
            }
            return 'X';
        }

        /// <summary>
        /// Vypise mapu.
        /// </summary>
        public void VypisSe()
        {
            for (int i = 0; i < Data.GetLength(1); i++)
            {
                for (int j = 0; j < Data.GetLength(0); j++)
                {
                    NastavBarvu(new Souradnice(j, i));
                    Console.Write(Data[j, i]);
                    Console.ResetColor();
                }
                Console.WriteLine();
            }

             /*// kod na vypisovani viditelnosti jedne osoby -- jen pro testovani
            if(posledni != null) { 
            Console.WriteLine();
            Console.WriteLine();
            for (int i = -2; i < 3; i++)
            {
                for (int j = -2; j < 3; j++)
                {
                    Console.Write(posledni[i, j]);
                    
                }
                Console.WriteLine();
            }
            }*/
        }

        private void NastavBarvu(Souradnice s)
        {
            if (JeNakupujici(s))
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.White;
                return;
            }
            if (JeZlodej(s))
            {
                Console.ForegroundColor = ConsoleColor.Black;
                Console.BackgroundColor = ConsoleColor.Cyan;
                return;
            }
            if (JeNabidka(s))
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.BackgroundColor = ConsoleColor.Black;
                return;
            }
        }

        /// <summary>
        /// Vrati kopii mapy s vyjmutyma vecma co maji tyto identifikatory.
        /// </summary>
        /// <param name="vylouceni"></param>
        /// <returns></returns>
        public char[,] VratMapu(IEnumerable<char> vylouceni)
        {
            char[,] kopie = (char[,])Data.Clone();
            for (int i = 0; i < kopie.GetLength(0); i++)
            {
                for (int j = 0; j < kopie.GetLength(1); j++)
                {
                    if (vylouceni.Contains(Data[i, j]))
                    {
                        kopie[i, j] = ' ';
                    }
                }
            }
            return kopie;
        }
        
        public bool JeVolno(Souradnice s)
        {
            return Data[s.X, s.Y] == ' ';
        }

        public bool JeZlodej(Souradnice s)
        {
            return Data[s.X, s.Y] >= 'A' && Data[s.X, s.Y] < 'X';
        }

        public bool JeNakupujici(Souradnice s)
        {
            return Data[s.X, s.Y] >= 'a' && Data[s.X, s.Y] <= 'z';
        }

        public bool JeNabidka(Souradnice s)
        {
            return Data[s.X, s.Y] >= '0' && Data[s.X, s.Y] <= '9';
        }

        /// <summary>
        /// Vrati cenu nabidky na teto pozici. Pokud tam neni nabidka, vrati -1.
        /// </summary>
        /// <param name="s">Sourdadnice mapy, na ktere hledam.</param>
        /// <returns>Cena nabidky nebo -1, pokud tam zadna neni.</returns>
        public int CenaNabidky(Souradnice s)
        {
            int cena = Data[s.X, s.Y] - '0';
            if(cena > 0 && cena < 10) // je to cislice
            {
                return cena;
            }
            return -1;
        }

        /// <summary>
        /// Vygeneruje jednu novou nabidku do mapy. Jeji pozici urcuje <see cref="generator"/>.
        /// </summary>
        public void GenerujNabidku()
        {
            generator.NovaNabidka(Data);
        }


    }

    class Vysek
    {
        private char[,] data;

        public Vysek(char[,] data)
        {
            this.data = data;
        }

        public char this[Souradnice s]
        {
            get
            {
                return Data[s.X + 2, s.Y + 2];
            }
            set
            {
                Data[s.X + 2, s.Y + 2] = value;
            }
        }

        public char this[int x, int y]
        {
            get
            {
                return Data[x + 2, y + 2];
            }
            set
            {
                Data[x + 2, y + 2] = value;
            }
        }

        public void Nevidet(Souradnice s)
        {
            Nevidet(s.X, s.Y);
        }
        public void Nevidet(int x, int y)
        {
            if (this[x, y] != 'X') this[x, y] = '?';
        }

        public char[,] Data { get => data; }
    }
}
