﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{
    class ViMaNakupujici : Nakupujici
    {
        private Souradnice GlobalSizeOfMap = new Souradnice(80, 20);//pomocna promena s velikosti mapy
        private Souradnice target = new Souradnice();//souradnice nabydky ke ktere nakupujici miri
        private char[,] mapa;
        private Souradnice globSouradnice;// aktualni globalni souradnice
        private List<Souradnice> path = new List<Souradnice>();// list s souradnicema pres ktere pujdu
        private int[,] pathMap = new int[5, 5];//mapa s poctem kroku za ktere jsme schopni se na urcitou souradnici dostat

        override public Smer Krok(char [,] mapa, int penize, Souradnice globSouradnice)
        {
            this.mapa = mapa;
            this.globSouradnice = globSouradnice;

            FindCurrentTarget();
            PathManager();

            return ReturnSmer(path.First());//opravit prvni se musi odendat
        }


        /// <summary>
        /// spousti vyhledavani cesty
        /// </summary>
        /// <returns>vraci list s potrebnimy kroky tak aby se nakupujici dostal na potrebne policko</returns>
        private List<Souradnice> PathManager()
        {
            for (int k = 0; k < 5; k++)
                for (int n = 0; n < 5; n++)
                    pathMap[k, n] = 100;
            pathMap[2, 2] = 0;

            FindPath(0);

            ChoosePath(target);
            ConvertPath();
            return path;
        }

        /// <summary>
        /// konvertuje cestu na pouzitelny vystup
        /// </summary>
        private void ConvertPath()
        {
            Souradnice souradnice = new Souradnice(2, 2);
            Souradnice sou, souStare;
            path.Reverse();
            sou = path[0];
            sou.X -= 2;
            sou.Y -= 2;
            path[0] = sou;

            for (int i = 1; i < path.Count; i++)
            {
                sou = path[i];
                souStare = path[i - 1];
                sou.X -= souStare.X;
                sou.Y -= souStare.Y;
                path[i] = sou;
            }
        }


        /// <summary>
        /// hleda nejrychlejsi cestu
        /// </summary>
        /// <param name="meziBod"></param>
        private void ChoosePath(Souradnice meziBod)
        {
            int minValue = 100;
            Souradnice minValueSouradnice = new Souradnice();

            for (int k = -1; k < 2 && k + meziBod.X >= 0 && k + meziBod.X < 5; k++)
                for (int n = -1; n < 2 && n + meziBod.Y >= 0 && n + meziBod.Y < 5; n++)
                    if (pathMap[meziBod.X + k, meziBod.Y + n] < minValue)
                    {
                        minValue = pathMap[meziBod.X + k, meziBod.Y + n];
                        minValueSouradnice = new Souradnice(meziBod.X + k, meziBod.Y + n);
                    }
            path.Add(minValueSouradnice);
            if (minValue > 0)
                ChoosePath(minValueSouradnice);
        }

        /// <summary>
        /// vytvari pathMap s poctem kroku potrebnych k dojiti na target policko
        /// </summary>
        /// <param name="vzdalenost"></param>
        void FindPath( int vzdalenost)
        {
            int pocetPrydanychVzdalenosti = 0;
            Souradnice mojeSouradnice = new Souradnice();

            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++)
                    if (pathMap[i, j] == vzdalenost)
                    {
                        mojeSouradnice.X = i;
                        mojeSouradnice.Y = j;
                        for (int k = -1; k < 2 && k + mojeSouradnice.X >= 0 && k + mojeSouradnice.X < 5; k++)
                            for (int n = -1; n < 2 && n + mojeSouradnice.Y >= 0 && n + mojeSouradnice.Y < 5; n++)
                            {
                                if (pathMap[mojeSouradnice.X + k, mojeSouradnice.Y + n] > vzdalenost + 1 && (mapa[mojeSouradnice.X + k, mojeSouradnice.Y + n] < 'A' || mapa[mojeSouradnice.X + k ,mojeSouradnice.Y + n ] > 'X') &&( mapa[mojeSouradnice.X + k, mojeSouradnice.Y + n] < 'a' || mapa[mojeSouradnice.X + k, mojeSouradnice.Y + n] > 'z'))
                                {
                                    pathMap[mojeSouradnice.X + k, mojeSouradnice.Y + n] = vzdalenost + 1;
                                    pocetPrydanychVzdalenosti++;
                                }
                            }
                    }
            if (pocetPrydanychVzdalenosti != 0)
                FindPath(++vzdalenost);
        }

        /// <summary>
        /// rozhoduje jestli ma cenu se zamerit na novou nabidku nebo ne
        /// </summary>
        private void FindCurrentTarget()
        {
            Souradnice newBestNabidka = FindBestNabidka(mapa);
            if (target.X >= 0 && target.Y >= 0 && target.X < 5 && target.Y < 5)
            {
                if (mapa[target.X, target.Y] - mapa[newBestNabidka.X, newBestNabidka.Y] >= 4)
                    target = newBestNabidka;
            }
            else
                target = newBestNabidka;
        }


        /// <summary>
        /// hleda aktualni nejlepsi nabidku nebo urci jiny cil
        /// </summary>
        /// <param name="mapa"></param>
        /// <returns></returns>
        private Souradnice FindBestNabidka(char [,] mapa)
        {
            Souradnice souradniceNabidky = new Souradnice();
            int minValue = 10;
            bool isNabidka = false;
            souradniceNabidky.X = 5;
            souradniceNabidky.Y = 5;

            for (int i = 0; i < 5; i++)
            {
                for(int j = 0; j < 5; j++)
                {
                    if(mapa[i,j] - 48 > 0 && mapa[i,j] - 48 < 10)
                        if (mapa[i,j] - 48 < minValue)
                        {
                            minValue = mapa[i, j] - 48;
                            souradniceNabidky.X = i;
                            souradniceNabidky.Y = j;
                            isNabidka = true;
                        }
                }
            }
            if (isNabidka == true)  
                return souradniceNabidky;
            else
            {
                if (globSouradnice.X < 40 && globSouradnice.Y < 10)
                {
                    target.X = 4;
                    target.Y = 4;
                    return souradniceNabidky;
                }
                if (globSouradnice.X > 40 && globSouradnice.Y < 10)
                {
                    target.X = 0;
                    target.Y = 4;
                    return souradniceNabidky;
                }
                if (globSouradnice.X < 40 && globSouradnice.Y > 10)
                {
                    target.X = 4;
                    target.Y = 0;
                    return souradniceNabidky;
                }
                else
                {
                    target.X = 0;
                    target.Y = 0;
                    return souradniceNabidky;
                }
            }
            
        }

        /// <summary>
        /// konvertuje Souradnici na smer
        /// </summary>
        /// <param name="nextStep"></param>
        /// <returns></returns>
        private Smer ReturnSmer(Souradnice nextStep)
        {
            Smer smer = new Smer((sbyte)nextStep.X, (sbyte)nextStep.Y );
            return smer;
        }
    }
}
