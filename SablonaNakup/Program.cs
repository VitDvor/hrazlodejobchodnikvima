﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SablonaNakup
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Nakupujici> nakupujici = new List<Nakupujici>();
            List<Zlodej> zlodeji = new List<Zlodej>();
            for (int i = 0; i < 3; i++)
            {
                nakupujici.Add(new ViMaNakupujici());
            }
            for (int i = 0; i < 3; i++)
            {
                zlodeji.Add(new NahodnyZlodej());
            }

            // TODO: pomoci nakupujici.Add nebo zlodeji.Add sem zaregistrujte svou postavu.

            GameManager gm = new GameManager(zlodeji, nakupujici);

            for (int i = 0; i < 200; i++)
            {
                Console.Clear();
                gm.VypisStatistiku();
                System.Threading.Thread.Sleep(500); //cekej chvili
                //Console.ReadKey(); // cekej na klavesu
                gm.Krok();
            }
            gm.VypisStatistiku();

        }
    }

    struct Souradnice
    {
        public int X, Y;

        public Souradnice(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static Souradnice operator+(Souradnice levy, Souradnice pravy)
        {
            return new Souradnice(levy.X + pravy.X, levy.Y + pravy.Y);
        }

    }
}
